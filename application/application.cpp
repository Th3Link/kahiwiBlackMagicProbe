#include "lowLevel/applicationHook.h"
#include "lowLevel/LED.hpp"
#include <chrono>
#include <message/Message.hpp>
#include <message/Queue.hpp>

class Dimmer : public message::Receiver<lowLevel::LED::Instance>
{
public:
    Dimmer(message::ReceiverQueue& q) : message::Receiver<lowLevel::LED::Instance>(q)
    {
        
    }
    virtual void receive(message::Message<lowLevel::LED::Instance>& m) final override
    {
        lowLevel::LED::value(m.data, 1-lowLevel::LED::value(m.data));
        message::Message<lowLevel::LED::Instance>::send(queue, *this, message::Event::UPDATE, 
            std::move(m.data), std::chrono::milliseconds(1000));
    }
};

// dont put large things on the stack
message::Queue<256, 256> queue;

void startApplication()
{
    lowLevel::LED::init(lowLevel::LED::Instance::L1);
    lowLevel::LED::init(lowLevel::LED::Instance::L2);
    lowLevel::LED::init(lowLevel::LED::Instance::L3);
    
    Dimmer dimmer(queue);
    message::Message<lowLevel::LED::Instance>::send(
        queue, dimmer, message::Event::UPDATE, 
        lowLevel::LED::Instance::L1, std::chrono::milliseconds(1000));
    
    message::Message<lowLevel::LED::Instance>::send(
        queue, dimmer, message::Event::UPDATE, 
        lowLevel::LED::Instance::L2, std::chrono::milliseconds(1333));

    message::Message<lowLevel::LED::Instance>::send(
        queue, dimmer, message::Event::UPDATE, 
        lowLevel::LED::Instance::L3, std::chrono::milliseconds(1666));
    
    while(true)
    {
        queue.dispatch();
    }
    
}
