FROM archlinux/base:latest

RUN pacman -Syu --noconfirm \
&& pacman -S --noconfirm arm-none-eabi-newlib arm-none-eabi-gcc arm-none-eabi-binutils gcc \
scons \
python python-pip \
&& pacman -Scc --noconfirm \
&& pip install sly intelhex
