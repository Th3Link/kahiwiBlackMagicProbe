#ifndef __NETWORK_NETWORK_HPP__
#define __NETWORK_NETWORK_HPP__

//include package intern

//include package extern

//include std::*, etl::*

namespace network
{
    enum class Protocol
    {  
        TCP, UDP
    };
        
    enum class Error
    {
        AWESOME,
        FAIL_NO_SPACE,
        FAIL_NOT_IMPLEMENTED,
        FAIL_NETWORK,
    };
        
    class Network
    {
        public:
            static void init()
            {
                
            }
            static void shutdown(void);
    };
}
#endif //__NETWORK_NETWORK_HPP__
