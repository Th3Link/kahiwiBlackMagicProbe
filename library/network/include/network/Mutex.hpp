#ifndef __GENERIC_MUTEX_HPP__
#define __GENERIC_MUTEX_HPP__

#include <memory>

namespace network {
class MutexImpl;

class Mutex
{
    public:
        Mutex();
        ~Mutex();
        bool lock();
        bool try_lock();
        void unlock();
    protected:

    private:
        std::unique_ptr<MutexImpl> mutex;
};
}
#endif //__GENERIC_MUTEX_HPP__
