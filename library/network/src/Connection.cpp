//include package intern
#include <network/Server.hpp>
#include <network/Connection.hpp>

//include package extern
#include <network/Network.hpp>

//include std::*

#define SSC_SIZE_LIMIT 1200

using namespace network;

Connection::Connection(Server& ns, std::array<unsigned int, IP_ARRAY_LEN> uId, unsigned int index) :
    Descriptor(ns.protocol, ns.port),
    pcb(nullptr),
    server(ns),
    closeSent(false),
    uniqueId(uId),
    index(index)
{

    data.reserve(SSC_SIZE_LIMIT);
}
Connection::~Connection()
{

}

Server& Connection::getServer()
{
    return server;
}

void Connection::close(Error& error)
{
    Network::closeConnection(*this, error);
}

bool Connection::operator==(const Connection& nc) {
    return hasSameService(nc) &&
        (uniqueId == nc.uniqueId) &&
        (index == nc.index);
}

void Connection::syncRemoteClosed(Error& error) {
    server.removeConnection(this, error);
}

void Connection::send(std::string& in, Error& error)
{
    Network::send(*this, in, error);
}

void Connection::recved(std::size_t n)
{
    Network::recved(*this, n);
}

/**
 * Needs to be thread safe
 */
void Connection::appendData(char* c, std::size_t n, Error& error)
{
    dataAccess.lock();
    
    if (data.size() + n > SSC_SIZE_LIMIT)
    {
        dataAccess.unlock();
        error = Error::FAIL_NO_SPACE;
        return;
    }
    
    data.append(c, n);
    dataAccess.unlock();
}

std::string Connection::execData(std::size_t (*f)(std::string&, std::string&), Error& error)
{
    std::string out;
    dataAccess.lock();
    std::size_t len = f(data, out);
    dataAccess.unlock();
    if (len == 0)
    {
        Network::recv(*this, error);
    }
    return out;
}

/**
 * This method is called out of a network thread scope! use message
 * queue to sync.
 */
void Connection::remoteClosed()
{
    if (!closeSent)
    {
        closeSent = true;
        uniqueId.fill(0);
        server.sync->requestConnectionRemoteClosedSync(this);
    }
}
