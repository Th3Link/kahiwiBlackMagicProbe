//include package intern
#include <network/Server.hpp>
#include <network/Connection.hpp>
#include <network/Network.hpp>

//include package extern

//include std::*
#include <algorithm>

using namespace network;

Server::Server(Protocol protocol, unsigned short port, std::shared_ptr<Sync> s) :
    Descriptor(protocol,port), pcb(nullptr), sync(s)
{
    
}

Server::~Server()
{
    
}

void Server::syncNewConnection(std::shared_ptr<Connection> connection) {

    connections.push_back(std::move(connection));
}

std::shared_ptr<Connection> Server::newConnection(std::array<unsigned int, IP_ARRAY_LEN> uId, unsigned int index)
{
    auto connection = std::shared_ptr<Connection>(new Connection(*this, uId, index));
    sync->requestNewConnectionSync(connection);
    return connection;
}

void Server::connectionIteration(void* me, void (*f)(void*, std::shared_ptr<Connection>), Error& error)
{   
    Network::accept(*this, error);
    for (auto c : connections)
    {
        f(me, c);
    }
}

void Server::bind(Error& error)
{
    Network::bind(*this, error);
}

void Server::closeAllConnections(Error& error)
{
    std::string ssc_close("{\"osc\":{\"state\":{\"close\":true}}}\r\n");    
    for (auto c : connections)
    {
        c.get()->send(ssc_close, error);
    }
    connections.clear();
}

void Server::removeConnection(Connection* nc, Error& error)
{
    connections.erase(std::find_if(connections.begin(), connections.end(), [nc](const std::shared_ptr<Connection> currentnc)
        { return currentnc.get() == nc; }));
    nc->close(error);    
}
