//include package intern

//include package extern
#include <network/Mutex.hpp>

//include std::*
#include <mutex>
using namespace network;

namespace network {
class MutexImpl
{
    public:
        MutexImpl() {};
        std::mutex handle;
    private:
};
}

Mutex::Mutex() :
    mutex(new MutexImpl())
{

}

Mutex::~Mutex()
{

}

bool Mutex::lock()
{
    mutex->handle.lock();
    return true;
}

bool Mutex::try_lock()
{
    return mutex->handle.try_lock();
}

void Mutex::unlock()
{
    mutex->handle.unlock();
}
