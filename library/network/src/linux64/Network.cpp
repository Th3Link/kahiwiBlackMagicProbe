#include <network/Network.hpp>
#include "asio.hpp"

//include std::*
#include <stdlib.h> //memcpy
#include <map>
#include <array>
#include <string.h>
#include <string>

using asio::ip::tcp;
using asio::ip::udp;

using namespace network;

#define BUF_LEN 500

//typedefs

unsigned int nextIndex = 0;

//static function declarations
void network_RecvTcp(Connection& nc, Error& error);

asio::io_service io_service;

void Network::init(std::array<uint8_t,6>& mac)
{
    printf("Network::init() with MAC: %x:%x:%x:%x:%x:%x\n",
        mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}

void Network::shutdown(void)
{
}

void Network::bind(Server& server, Error& error)
{
    if (server.protocol == Descriptor::PROTOCOL_TCP)
    {
        int newPort = server.port;
        
        tcp::acceptor* acceptor = new tcp::acceptor(io_service);
        tcp::endpoint endpoint(tcp::v6(), newPort);
        acceptor->open(endpoint.protocol());
        acceptor->set_option(tcp::acceptor::reuse_address(false));
        acceptor->bind(endpoint);
        acceptor->listen();
        
        server.pcb = acceptor;
        server.port = newPort;
    }
    else if (server.protocol == Descriptor::PROTOCOL_UDP)
    {
        server.pcb = new udp::socket(io_service, udp::endpoint(udp::v6(), server.port));
    }
}

void Network::accept(Server& server, Error& error) 
{
    // can only be called from an TCP connection
    asio::error_code ec;

    asio::ip::tcp::endpoint endpoint;
    asio::ip::tcp::socket* socket_ptr = new asio::ip::tcp::socket(io_service);
    static_cast<tcp::acceptor*>(server.pcb)->non_blocking(true);
    static_cast<tcp::acceptor*>(server.pcb)->accept(*socket_ptr, endpoint, ec);
 
    if (!ec) {
        //get byte array from endpoint
        std::array<unsigned char, 16> arr = endpoint.address().to_v6().to_bytes();
        //copy data part to uniqueId (ipv6 address)
        std::array<unsigned int, IP_ARRAY_LEN> uid;
        memcpy(uid.data(), arr.data(), sizeof(unsigned int)*IP_ARRAY_LEN);
    
        auto connection = server.newConnection(uid, nextIndex++);
        connection->pcb = socket_ptr;
        return;
    }
    delete(socket_ptr);
    error = Error::FAIL_NETWORK;
}

void Network::recv(Connection& nc, Error& error)
{
    
    if (nc.protocol == Descriptor::PROTOCOL_TCP)
    {
        network_RecvTcp(nc, error);
    }
    else if (nc.protocol == Descriptor::PROTOCOL_UDP)
    {
        error = Error::FAIL_NOT_IMPLEMENTED;
    }
}

void network_RecvTcp(Connection& nc, Error& error)
{
    asio::error_code ec;
    static_cast<asio::ip::tcp::socket*>(nc.pcb)->non_blocking(true);
    char buf[BUF_LEN];
    std::size_t rxLen = static_cast<asio::ip::tcp::socket*>(nc.pcb)->read_some(asio::buffer(buf, BUF_LEN), ec);
    
    if (asio::error::eof == ec ||
        asio::error::connection_aborted == ec ||
        asio::error::connection_refused == ec ||
        asio::error::connection_reset == ec)
    {
        nc.remoteClosed();
        error = Error::FAIL_NETWORK;
        return;
    }
    else if (ec)
    {
        error = Error::FAIL_NETWORK;
        return;
    }
    
    nc.appendData(buf, rxLen, error);
}

void Network::send(Connection& nc, std::string& in, Error& error)
{
    if (nc.protocol == Descriptor::PROTOCOL_TCP)
    {
        asio::error_code ec;
        asio::write(*static_cast<asio::ip::tcp::socket*>(nc.pcb), asio::buffer(in), ec);
        
        if (ec) {
            error = Error::FAIL_NETWORK;  
        }
    }
    else if (nc.protocol == Descriptor::PROTOCOL_UDP)
    {
        error = Error::FAIL_NOT_IMPLEMENTED;
    }
}

void Network::recved(Connection& nc, unsigned short len)
{
}

void Network::closeServer(Server& server, Error& error)
{
    if (server.protocol == Descriptor::PROTOCOL_TCP)
    {
        asio::error_code ec;
        static_cast<tcp::acceptor*>(server.pcb)->close(ec);
        if (ec)
        {
            error = Error::FAIL_NETWORK;
        }
    }
    else if (server.protocol == Descriptor::PROTOCOL_UDP)
    {
        error = Error::FAIL_NOT_IMPLEMENTED;
    }
}

void Network::closeConnection(Connection& nc, Error& error) {
    if (nc.protocol == Descriptor::PROTOCOL_TCP)
    {
        asio::error_code ec;
        static_cast<asio::ip::tcp::socket*>(nc.pcb)->close(ec);
        if (ec)
        {
            error = Error::FAIL_NETWORK;
        }
    }
    else if (nc.protocol == Descriptor::PROTOCOL_UDP)
    {
        error = Error::FAIL_NOT_IMPLEMENTED;
    }
}
